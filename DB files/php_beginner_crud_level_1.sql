-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2018 at 10:37 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_beginner_crud_level_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `image` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `image`, `created`, `modified`) VALUES
(9, 'axaxax', 'hiiiiiiiiiiiii', 12, '', '2018-08-10 08:17:51', '2018-08-10 06:17:51'),
(10, 'test', 'test', 0, '', '2018-08-10 20:09:09', '2018-08-10 18:09:09'),
(13, 'nicks', 'sdasdasd', 12, '', '2018-08-10 22:09:11', '2018-08-10 20:09:11'),
(14, 'nicks', 'sdasdasd', 12, '', '2018-08-10 22:09:37', '2018-08-10 20:09:37'),
(15, 'nicks', 'hello', 123, '', '2018-08-10 22:09:55', '2018-08-10 20:09:55'),
(16, 'nicks', 'hello', 123, '', '2018-08-10 22:23:56', '2018-08-10 20:23:56'),
(17, 'Nicks', 'Bhavin', 0, 'd160637323db13317c97c4233a829eb9aaf9a5ac-photo6105125355489699853.jpg', '2018-08-10 22:24:19', '2018-08-10 20:24:19'),
(18, 'knarky', 'upload form', 1000, 'b5362f776a0e807ebadd7f100a3aebe12a6378f9-Screenshot_2018-06-22-17-01-08-937_com.justinmind.androidapp.png', '2018-08-10 22:25:33', '2018-08-10 20:25:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
